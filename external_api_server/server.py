import itertools
from sanic import Sanic
from sanic.response import text
from sanic.exceptions import abort


STATUS_OK = 200
STATUS_UNAVAILABLE = 503

app = Sanic()


def _status_code_maker():
    codes = [
        STATUS_OK,
        STATUS_OK,
        STATUS_UNAVAILABLE,
        STATUS_OK,
        STATUS_UNAVAILABLE,
        STATUS_UNAVAILABLE,
        STATUS_UNAVAILABLE,
        STATUS_OK,
    ]
    for code in itertools.cycle(codes):
        yield code

status_code_maker = _status_code_maker()



@app.route("/")
async def test(request):
    code = next(status_code_maker)
    if code == STATUS_UNAVAILABLE:
        abort(STATUS_UNAVAILABLE)
    else:
        return text("ok")
