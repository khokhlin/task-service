FROM python:3.7-alpine
RUN apk add --no-cache gcc make musl-dev linux-headers
WORKDIR /task_service
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
