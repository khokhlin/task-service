import json
import time
import unittest
import urllib.request


class TestApi(unittest.TestCase):

    @staticmethod
    def make_request(url):
        with urllib.request.urlopen(url) as f:
            return json.loads(f.read().decode())

    def test_can_create_task(self):

        task_ids = [
            self.make_request("http://localhost:8082/call?task=1")["task_id"],
            self.make_request("http://localhost:8082/call?task=2")["task_id"],
            self.make_request("http://localhost:8082/call?task=3")["task_id"],
            self.make_request("http://localhost:8082/call?task=4")["task_id"],
        ]

        time.sleep(1)

        for task_id in task_ids:
            resp = self.make_request("http://localhost:8082/status?task_id=%s" % task_id)
            self.assertEqual(resp["task_id"], task_id)
            self.assertEqual(resp["status"], "completed")
       
 
if __name__ == '__main__':
    unittest.main()
