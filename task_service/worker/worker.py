import json
import asyncio
from functools import partial
import aioamqp
import aiohttp
import motor.motor_asyncio


QUEUE_NAME = "tasks"
MONGO_URI = 'mongodb://root:task_service@mongo:27017'
EXTERNAL_API_URL = "http://external_api:8081/"

STATUS_COMPLETED = "completed"
STATUS_REJECTED = "rejected"
STATUS_PENDING = "pending"


async def save_payload(db, payload, status=STATUS_PENDING):
    task_data = payload.copy()
    task_data["status"] = status
    coll = db.test_collection
    document = await coll.find_one({'task_id': task_data["task_id"]})
    if document:
        _id = document["_id"]
        task_data["attempts"] = document["attempts"] + 1
        await coll.replace_one({"_id": document["_id"]}, task_data)
    else:
        task_data["attempts"] = 1
        await coll.insert_one(task_data)


async def task_callback(db, channel, body, envelope, properties):
    payload = json.loads(body.decode())
    async with aiohttp.ClientSession() as session:
        async with session.get(EXTERNAL_API_URL, params=payload) as resp:
            status = resp.status
            if status == 200:
                await save_payload(db, payload, status=STATUS_COMPLETED)
            elif status == 503:
                await channel.basic_publish(
                    payload=json.dumps(payload),
                    exchange_name="",
                    routing_key=QUEUE_NAME
                )
                await save_payload(db, payload, status=STATUS_REJECTED)


async def work(waiter):
    transport, protocol = await aioamqp.connect(host="rabbitmq")
    channel = await protocol.channel()

    mongo_client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URI)
    db = mongo_client.test_database
    callback = partial(task_callback, db)

    await asyncio.wait_for(
        channel.basic_consume(callback, QUEUE_NAME, no_ack=True), timeout=5)
    await waiter.wait()

    await protocol.close()
    transport.close()


def main():
    loop = asyncio.get_event_loop()
    try:
        waiter = asyncio.Event()
        loop.run_until_complete(work(waiter))
    except KeyboardInterrupt:
        waiter.set()
