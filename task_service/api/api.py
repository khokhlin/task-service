import json
import uuid
import aioamqp
import motor.motor_asyncio
from sanic import Sanic
from sanic import response
from sanic.exceptions import abort


MONGO_URI = 'mongodb://root:task_service@mongo:27017'
QUEUE_NAME = "tasks"

app = Sanic()


def _make_task_id():
    return uuid.uuid4().hex


@app.route("/call")
async def call(request):
    task_data = request.args["task"][0]
    task_id = _make_task_id()
    payload = {
        "task_data": task_data,
        "task_id": task_id,
    }

    await app.channel.basic_publish(
        payload=json.dumps(payload),
        exchange_name="",
        routing_key=QUEUE_NAME
    )
    return response.json({"task_id": task_id})


@app.route("/status")
async def check_status(request):
    task_id = request.args["task_id"][0]
    document = await app.db.test_collection.find_one({'task_id': {'$eq': task_id}})
    if not document:
        abort(404, "Task doesn't exist")
    document.pop("_id")
    return response.json(document)


@app.listener('before_server_start')
async def setup_connections(app, loop):
    transport, protocol = await aioamqp.connect(host="rabbitmq")
    channel = await protocol.channel()
    app.transport = transport
    app.protocol = protocol
    app.channel = channel
    await channel.queue_declare(queue_name=QUEUE_NAME)

    mongo_client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URI)
    app.db = mongo_client.test_database


@app.listener('before_server_stop')
async def close_connections(app, loop):
    await app.protocol.close()
    app.transport.close()
